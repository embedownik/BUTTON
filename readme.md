# BUTTON library

Simple module for buttons.

Platform independent - all HW access passed in config from higher software layer.

This module works as separate FreeRTOS task.

## Requirements

FreeRTOS - right now there is no additional OSAL.

## Usage

1. Prepare GetState functions for buttons:
```c
bool button0_GetState(void)
{
    bool retVal = false;

    if(HAL_GPIO_ReadPin(BUTTON0_GPIO_Port, BUTTON0_Pin) == GPIO_PIN_RESET)
    {
        retVal = true;
    }

    return retVal;
}

bool button1_GetState(void)
{
    bool retVal = false;

    if(HAL_GPIO_ReadPin(BUTTON1_GPIO_Port, BUTTON1_Pin) == GPIO_PIN_RESET)
    {
        retVal = true;
    }

    return retVal;
}

bool button2_GetState(void)
{
    bool retVal = false;

    if(HAL_GPIO_ReadPin(BUTTON2_GPIO_Port, BUTTON2_Pin) == GPIO_PIN_RESET)
    {
        retVal = true;
    }

    return retVal;
}
```

2. Create array with buttons settings:

```c
Button_SingleButton_Config_s buttonsArray[] =
{
    {
       .getState = button0_GetState,
       .debounceTimeHigh = 5,
       .debounceTimeLow = 5,
       .longPressTime = 0,
    },
    {
       .getState = button1_GetState,
       .debounceTimeHigh = 5,
       .debounceTimeLow = 5,
       .longPressTime = 0,
    },
    {
       .getState = button2_GetState,
       .debounceTimeHigh = 5,
       .debounceTimeLow = 5,
       .longPressTime = 0,
    },
};
```
3. Implement callback - this will be called after any event from any button:

```c
void buttonsEventsCallback(uint8_t buttonNumber, ButtonEvent_e event)
{
    AppEvent_NewButtonCLick(buttonNumber, event);
}
```

4. Group all into single config struct:

```c
ButtonsConfig_s buttonsConfig = 
{
    .buttonsArray = buttonsArray,
    .buttonsNumber = (sizeof(buttonsArray)/sizeof(buttonsArray[0])),
    .measInterval = 10,
    .eventCallback = buttonsEventsCallback,
    .buttonTaskPriority = BUTTONS_TASK_PRIORITY,
    .buttonTaskStackSize = BUTTONS_TASK_STACK_SIZE,
};
```
5. Pass config struct to Init function:

```c
BUTTONS_Init(&buttonsConfig);
```

Init function creates separate RTOS task - you can create multiple instances of ConfigStruct and use this module multiple times in single application.

## Alternative option 1 - no RTOS

To use module without RTOS - in "board.h" file - define symbol:

```c
#define BUTTON_DEF_NO_RTOS
```

You need to use interface:

```c
void BUTTONS_Tick(ButtonsConfig_s* config);
```

inside your super loop - instead of separate task.

## Alternative option 2 - events queue instead of callbacks

This method is prefered - because this is easy way to separate "callback" context.

Problem with first method - callbacks are called/executed from button task context - this is quite hard to maintain and
user is responsible for calculating stack sizes.

How to use it - do not initialize callback and stackSize, additional field to init is "eventsQueueSize".

Then implement separate task for some kind of event dispather and call:

```c
/**
 * Get event from internal event Queue. To avoid calling callbacks from button task context.
 */
bool BUTTONS_GetEventFromQueue(ButtonsConfig_s* config, ButtonEvent_s* event, TickType_t waitTime);
```


## Events example

![](docs/events.png)

Where:

 - P - pressed Event
 - LP - longPress Event
 - RP - repeat Event
 - REL - release Event

Events for these settings:

```c
Button_SingleButton_Config_s buttonsArray[] =
{
    {
       .getState = button0_GetState,
       .debounceTimeHigh = 3U,
       .debounceTimeLow  = 3U,
       .longPressTime    = 5U,
       .repeatTime       = 3U,
       .releaseEventActive = true,
    },
};
```
