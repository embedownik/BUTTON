/*
 * button.c
 *
 *      Author: Embedownik
 */

#include <button.h>

#ifndef BUTTON_DEF_NO_RTOS
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#define BTN_STATIC_FUNCTION static

#else
#define BTN_STATIC_FUNCTION ()
#endif

static const uint16_t COUNTERS_MAX_VALUE = UINT16_MAX;

/**
 * Run callback or send event to queue.
 */
static void runEventCallback(ButtonsConfig_s* config, uint8_t buttonNumber, ButtonEvent_e event)
{
    if(config->eventCallback)
    {
        config->eventCallback(buttonNumber, event);
    }
    else
    {
#ifndef BUTTON_DEF_NO_RTOS

        ButtonEvent_s eventToSend = {
            .buttonNumber = buttonNumber,
            .event = event,
        };

        xQueueSend(config->eventsQueue, &eventToSend, 10U);
#endif
    }
}

/**
 * Check states of all buttons in given config struct.
 */
static void BUTTONS_GetStates(ButtonsConfig_s* config)
{
    for(uint8_t i = 0U; i < config->buttonsNumber; i++)
    {
        config->buttonsArray[i].internal.actualState = config->buttonsArray[i].getState();
    }
}

/**
 * Check events of all buttons in given config struct.
 */
static void BUTTONS_CheckEvents(ButtonsConfig_s* config)
{
    for(uint8_t buttonNumber = 0U; buttonNumber < config->buttonsNumber; buttonNumber++)
    {
        Button_SingleButton_Config_s* actualButton = &config->buttonsArray[buttonNumber];

        if(actualButton->internal.actualState)
        {
            actualButton->internal.l_counter = 0U;

            /* variable overflow protection */
            if(actualButton->internal.h_counter < COUNTERS_MAX_VALUE)
            {
                actualButton->internal.h_counter++;
            }

            /* check short press event */
            if(actualButton->internal.h_counter == actualButton->debounceTimeHigh)
            {
                actualButton->internal.buttonPressed = true;
                runEventCallback(config, buttonNumber, ButtonEvent_PRESSED);
            }

            /* check long press event */
            if(actualButton->longPressTime != 0U)
            {
                if(actualButton->internal.h_counter == actualButton->longPressTime)
                {
                    runEventCallback(config, buttonNumber, ButtonEvent_PRESSED_LONG);
                }
            }

            /* check repeat press event */
            if(actualButton->repeatTime != 0U)
            {
                if(actualButton->internal.h_counter >= actualButton->longPressTime)
                {
                    actualButton->internal.h_counter_LongPressAndRepeat++;

                    if(actualButton->internal.h_counter_LongPressAndRepeat == actualButton->repeatTime)
                    {
                        actualButton->internal.h_counter_LongPressAndRepeat = 0U;
                        runEventCallback(config, buttonNumber, ButtonEvent_PRESSED_REPEAT);
                    }
                }
            }
        }
        else
        {
            /* variable overflow protection */
            if(actualButton->internal.l_counter < COUNTERS_MAX_VALUE)
            {
                actualButton->internal.l_counter++;
            }

            if(actualButton->internal.l_counter == actualButton->debounceTimeLow)
            {
                actualButton->internal.h_counter = 0U;
                actualButton->internal.h_counter_LongPressAndRepeat = 0U;

                if(actualButton->internal.buttonPressed == true)
                {
                    /*
                     * If button earlier was pressed - fire "RELEASED" event
                     * this way we can avoid "RELEASED" events after power on or after some noise
                     */
                    actualButton->internal.buttonPressed = false;
                    if(actualButton->releaseEventActive)
                    {
                        runEventCallback(config, buttonNumber, ButtonEvent_RELEASED);
                    }
                }
            }
        }
    }
}

BTN_STATIC_FUNCTION void BUTTONS_Tick(ButtonsConfig_s* config)
{
    BUTTONS_GetStates(config);

    BUTTONS_CheckEvents(config);
}

static void BUTTONS_Task(void* params)
{
    ButtonsConfig_s* config = (ButtonsConfig_s*)params;

    while(true)
    {
        BUTTONS_Tick(config);

        /* this way isn't "time perfect" - but for just buttons is good enough */
        vTaskDelay(config->measInterval);
    }
}

static bool initRTOSComponents(ButtonsConfig_s* config)
{
#ifndef BUTTON_DEF_NO_RTOS
    bool     initSuccess = true;

    uint16_t stackSize = 0U;

    if(config->buttonTaskStackSize == 0U)
    {
        stackSize = BUTTON_DEFAULT_STACK_SIZE;
    }
    else
    {
        stackSize = config->buttonTaskStackSize;
    }

    BaseType_t xReturned = xTaskCreate(BUTTONS_Task, /* Function that implements the task. */
        "BTN",                                       /* Text name for the task. */
        stackSize,                                   /* Stack size in words, not bytes. */
        (void*)config,                               /* Parameter passed into the task. */
        config->buttonTaskPriority,                  /* Priority at which the task is created. */
        NULL);                                       /* Used to pass out the created task's handle. */

    if(xReturned != pdPASS)
    {
        initSuccess = false;
    }
    else
    {
        config->eventsQueue = xQueueCreate(config->eventsQueueSize, sizeof(ButtonEvent_s));

        if(config->eventsQueue == NULL)
        {
            initSuccess = false;
        }
    }
#endif

    return initSuccess;
}

bool BUTTONS_Init(ButtonsConfig_s* config)
{
    bool initSuccess = true;

    initSuccess = initRTOSComponents(config);

    return initSuccess;
}

#ifndef BUTTON_DEF_NO_RTOS
bool BUTTONS_GetEventFromQueue(ButtonsConfig_s* config, ButtonEvent_s* event, TickType_t waitTime)
{
    bool retVal = false;

    if(xQueueReceive(config->eventsQueue, &event, waitTime))
    {
        retVal = true;
    }

    return retVal;
}
#endif
