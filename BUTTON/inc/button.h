/*
 * button.h
 *
 *      Author: Embedownik
 */

#ifndef EMBEDOWNIK_BUTTON_BUTTON_INC_BUTTON_H_
#define EMBEDOWNIK_BUTTON_BUTTON_INC_BUTTON_H_

#include <stdint.h>
#include <stdbool.h>

#ifndef BUTTON_DEF_NO_RTOS
#include <FreeRTOS.h>
#include <queue.h>

/**
 *  default stack size when internal event queue is used
 *  calculated on project with STM32 and ST HAL as buttons callbacks
 **/
#define BUTTON_DEFAULT_STACK_SIZE 128U
#endif

/* all possible buttons events */
typedef enum
{
    ButtonEvent_PRESSED,
    ButtonEvent_PRESSED_LONG,
    ButtonEvent_PRESSED_REPEAT,
    ButtonEvent_RELEASED,
} ButtonEvent_e;

/* typedef of function to check button state - should return true if button is pressed */
typedef bool (*getButtonState_cb)(void);

/* typedef for callback after detecting button event */
typedef void (*buttonEventCallback)(uint8_t buttonNumber, ButtonEvent_e event);

/* typedef for event data in single struct */
typedef struct
{
    ButtonEvent_e event;
    uint8_t       buttonNumber;
} ButtonEvent_s;

/*
 * internal button counters and flags used by module
 *
 * but this shouldn't be visible/accessible to user
 * but not as opaque struct - to avoid dynamic allocation
 */
typedef struct
{
    uint16_t h_counter;                    /* counter for pressed time */
    uint16_t h_counter_LongPressAndRepeat; /* counter for long press and "repeat" */
    uint16_t l_counter;                    /* counter for released state time */
    bool     buttonPressed;                /* flag for release event */
    bool     actualState; /* copy of state - used to separate "getStates" and check events phases in task */
} Button_internalParametes_s;

/**
 * Struct with all single button parameters.
 * Note: measure unit for "Time" is tick from settings -> "measIntervalTime"
 */
typedef struct
{
    /* to configure by user */
    getButtonState_cb getState; /* callback for getState function for button */

    uint8_t           debounceTimeHigh;
    uint8_t           debounceTimeLow;
    uint8_t           longPressTime;
    uint8_t           repeatTime;
    bool              releaseEventActive;

    /* this part is used by library - this way to avoid dynamic allocation */
    Button_internalParametes_s internal;
} Button_SingleButton_Config_s;

/**
 * Config for module - this way user can easily add any number of buttons.
 */
typedef struct
{
    Button_SingleButton_Config_s* buttonsArray;
    uint8_t                       buttonsNumber;
    buttonEventCallback           eventCallback;
    uint8_t                       measInterval;
#ifndef BUTTON_DEF_NO_RTOS
    uint16_t      buttonTaskStackSize;
    uint16_t      buttonTaskPriority;

    QueueHandle_t eventsQueue;     /* queue for events, shouldn't be visible to user :/ */
    uint8_t       eventsQueueSize; /* size for events queue elements */
#endif
} ButtonsConfig_s;

/**
 * Init function - this function will create separate task based on given config struct.
 *
 * Given struct is also used as "internal" data and will be modified by library.
 */
bool BUTTONS_Init(ButtonsConfig_s* config);

#ifdef BUTTON_DEF_NO_RTOS
/*
 * Function to cyclic call - from some kind of timer callback when RTOS for BUTTONS is disabled.
 */
void BUTTONS_Tick(ButtonsConfig_s* config);
#endif

#ifndef BUTTON_DEF_NO_RTOS
/**
 * Get event from internal event Queue. To avoid calling callbacks from button task context.
 */
bool BUTTONS_GetEventFromQueue(ButtonsConfig_s* config, ButtonEvent_s* event, TickType_t waitTime);
#endif

#endif
